/*
 * =====================================================================================
 *
 *       Filename:  TerminalPlotter.cpp
 *
 *    Description: Contains the TerminalPlotter class methods 
 *
 *        Version:  1.0
 *        Created:  09/11/2020 11:45:01 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alexander Brown (), alex.brown7711@aggiemail.usu.edu
 *   Organization:  Utah State University
 *
 * =====================================================================================
 */

// Custom Header Files
#include "termviz/TerminalPlotter.h"

// System Header Files
#include <cstdlib>
#include <cmath>
#include <fstream>

using namespace termviz;

/*-----------------------------------------------------------------------------
 * PUBLIC 
 *-----------------------------------------------------------------------------*/

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: TerminalPlotter
 * Description:  
 *--------------------------------------------------------------------------------------
 */
TerminalPlotter::TerminalPlotter(ros::NodeHandle* const node):
  m_node(node),
  m_scale(1),
  m_resolution(0),
  m_range{50,50,0,0},
  m_index_size{0,0},
  m_zoom(4)
{
  m_odom_sub = m_node->subscribe("/robot_poses", 1, &TerminalPlotter::odomCallback, this);
  
  termviz::MapParser parser("/home/alex/Code/ROS/Patrol/src/termviz/src/scenario_world.yaml");
  m_wall = parser.parse();
  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: Plot
 * Description:  
 *--------------------------------------------------------------------------------------
 */
void TerminalPlotter::Plot()
{
  // Determine Scale
  determineScale();

  // Convert Range to Integer Indexes
  rangeToIndex();

  // Draw
  draw();

  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: ~TerminalPlotter
 * Description:  
 *--------------------------------------------------------------------------------------
 */
TerminalPlotter::~TerminalPlotter()
{}

/*-----------------------------------------------------------------------------
 * PRIVATE 
 *-----------------------------------------------------------------------------*/

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: roboutFound
 * Description:  Determine if any robot is within drawing range 
 *--------------------------------------------------------------------------------------
 */
bool TerminalPlotter::robotFound(int& robot, int x, int y)
{
  bool robot_found = false;

  for (int r = 0; r < m_odom->vehicles.size(); ++r)
  {
    if (x >= (int)(m_odom->vehicles[r].pose.pose.position.x * m_zoom - m_resolution) &&
        x <= (int)(m_odom->vehicles[r].pose.pose.position.x * m_zoom + m_resolution) &&
        y >= (int)(m_odom->vehicles[r].pose.pose.position.y * m_zoom - m_resolution) &&
        y <= (int)(m_odom->vehicles[r].pose.pose.position.y * m_zoom + m_resolution))
    {
      robot       = r;
      robot_found = true;
      break;
    }
  }

  return robot_found;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: odomCallback
 * Description:  Callback method to get odom data. 
 *--------------------------------------------------------------------------------------
 */
void TerminalPlotter::odomCallback(const mv_msgs::VehiclePoses::ConstPtr& odom)
{
  m_odom = odom;
  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: determineScale
 * Description:  Caculate the scale of the graph (zoom)
 *--------------------------------------------------------------------------------------
 */
void TerminalPlotter::determineScale()
{
  m_range.x_scaled = m_range.x * m_scale; 
  m_range.y_scaled = m_range.y * m_scale; 
  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: rangeToIndex
 * Description:  Convert the given range into discrete indicies 
 *--------------------------------------------------------------------------------------
 */
void TerminalPlotter::rangeToIndex()
{
  // TODO: Make a better way to determine indexing. Maybe given a certain amount of steps
  //       try to discritize into N steps as evenly as possible?
  m_index_size.x = (int) m_range.x_scaled;
  m_index_size.y = (int) m_range.y_scaled;
  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: draw
 * Description:  Draw the plot in the console 
 *--------------------------------------------------------------------------------------
 */
void TerminalPlotter::draw()
{
  if (!m_odom)
    return;
  
  drawWall();
 
  if (!m_map_walls.empty())
  {
    drawRobot();

    std::cout << m_map; 

    printf("\nScale: %i\nResolution: %i\n", m_scale, m_resolution);
  }

  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: drawRobot
 * Description:  Draw the robot 
 *--------------------------------------------------------------------------------------
 */
void TerminalPlotter::drawRobot()
{
  m_map.clear();
  m_map = m_map_walls;
  
  int idx   = 0;
  int robot = 0;

  for (int i = -m_index_size.y/2; i < m_index_size.y/2; ++i)
  {
    for (int j = -m_index_size.x/2; j < m_index_size.x/2; ++j)
    {
      if (robotFound(robot, j, i))
        m_map[idx] = *(std::to_string(robot).c_str());

        ++idx;
    }
    ++idx;
  }
 
  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: isWall
 * Description:  Draw the walls
 *--------------------------------------------------------------------------------------
 */
void TerminalPlotter::drawWall()
{
  m_map_walls.clear();

  for (int i = -m_index_size.y/2; i < m_index_size.y/2; ++i)
  {
    for (int j = -m_index_size.x/2; j < m_index_size.x/2; ++j)
    {
      if (wallFound(j, i))
        m_map_walls.append("*");
      else
        m_map_walls.append(" ");
    }
    m_map_walls.append("\n");
  }

  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: parseMap
 * Description:  Takes in a yaml file and parses out the walls 
 *--------------------------------------------------------------------------------------
 */
void TerminalPlotter::parseMap()
{
  std::ifstream map ("~/Code/ROS/Patrol/src/termviz/src/scenario_world.yaml");
}


/*
 *--------------------------------------------------------------------------------------
 *       Class:  TerminalPlotter
 *      Method:  TerminalPlotter :: wallFound
 * Description:  Determines if a wall was found at the given (x,y) coordinate 
 *--------------------------------------------------------------------------------------
 */
bool TerminalPlotter::wallFound(int x, int y)
{
  bool wall_found = false;
  float low_x     = 0;
  float high_x    = 0;
  float low_y     = 0;
  float high_y    = 0;

  for (int i = 0; i < m_wall.size(); ++i)
  {
    low_x  = m_wall[i].first.x <  m_wall[i].second.x ? m_wall[i].first.x : m_wall[i].second.x;
    low_y  = m_wall[i].first.y <  m_wall[i].second.y ? m_wall[i].first.y : m_wall[i].second.y;
    high_x = m_wall[i].first.x >= m_wall[i].second.x ? m_wall[i].first.x : m_wall[i].second.x;
    high_y = m_wall[i].first.y >= m_wall[i].second.y ? m_wall[i].first.y : m_wall[i].second.y;

    if (x >= (int)(low_x  * m_zoom - m_resolution) &&
        x <= (int)(high_x * m_zoom + m_resolution) &&
        y >= (int)(low_y  * m_zoom - m_resolution) &&
        y <= (int)(high_y * m_zoom + m_resolution))
    {
      wall_found = true;
      break;
    }
  }

  return wall_found;
}
