let SessionLoad = 1
let s:so_save = &g:so | let s:siso_save = &g:siso | setg so=0 siso=0 | setl so=-1 siso=-1
let v:this_session=expand("<sfile>:p")
silent only
cd ~/Code/ROS/Patrol/src/termviz/src
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +1 TerminalPlotter.cpp
badd +41 termviz.cpp
badd +1 ~/Code/ROS/Patrol/src/termviz/include/termviz/TerminalPlotter.h
badd +1 MapParser.cpp
badd +1 ~/Code/ROS/Patrol/src/termviz/include/termviz/MapParser.h
badd +38 ~/Code/ROS/Patrol/src/termviz/include/termviz/DataStructures.h
badd +1 term://~/Code/ROS/Patrol/src/termviz/src//914736:/bin/zsh
badd +111 ~/Code/ROS/Patrol/src/termviz/CMakeLists.txt
badd +46 ~/Code/ROS/Patrol/src/termviz/package.xml
badd +1 ~/Code/ROS/Patrol/src/termviz/test.yaml
badd +1 scenario_world.yaml
badd +1 TerminalPlotter.hpp
badd +76 ~/Code/ROS/Patrol/src/termviz/launch/multi_patrol_termviz.launch
argglobal
%argdel
$argadd TerminalPlotter.cpp
set stal=2
edit termviz.cpp
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 33 - ((10 * winheight(0) + 19) / 38)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
33
normal! 05|
tabedit TerminalPlotter.cpp
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 98 + 74) / 148)
exe 'vert 2resize ' . ((&columns * 49 + 74) / 148)
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 207 - ((11 * winheight(0) + 19) / 38)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
207
normal! 0
wincmd w
argglobal
if bufexists("~/Code/ROS/Patrol/src/termviz/include/termviz/TerminalPlotter.h") | buffer ~/Code/ROS/Patrol/src/termviz/include/termviz/TerminalPlotter.h | else | edit ~/Code/ROS/Patrol/src/termviz/include/termviz/TerminalPlotter.h | endif
if &buftype ==# 'terminal'
  silent file ~/Code/ROS/Patrol/src/termviz/include/termviz/TerminalPlotter.h
endif
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 71 - ((27 * winheight(0) + 19) / 38)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
71
let s:c = 45 - ((36 * winwidth(0) + 24) / 49)
if s:c > 0
  exe 'normal! ' . s:c . '|zs' . 45 . '|'
else
  normal! 045|
endif
wincmd w
exe 'vert 1resize ' . ((&columns * 98 + 74) / 148)
exe 'vert 2resize ' . ((&columns * 49 + 74) / 148)
tabedit MapParser.cpp
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 74 + 74) / 148)
exe 'vert 2resize ' . ((&columns * 73 + 74) / 148)
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 240 - ((27 * winheight(0) + 19) / 38)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
240
normal! 033|
wincmd w
argglobal
if bufexists("~/Code/ROS/Patrol/src/termviz/include/termviz/MapParser.h") | buffer ~/Code/ROS/Patrol/src/termviz/include/termviz/MapParser.h | else | edit ~/Code/ROS/Patrol/src/termviz/include/termviz/MapParser.h | endif
if &buftype ==# 'terminal'
  silent file ~/Code/ROS/Patrol/src/termviz/include/termviz/MapParser.h
endif
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 3 - ((1 * winheight(0) + 19) / 38)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
3
normal! 02|
wincmd w
exe 'vert 1resize ' . ((&columns * 74 + 74) / 148)
exe 'vert 2resize ' . ((&columns * 73 + 74) / 148)
tabedit scenario_world.yaml
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
wincmd _ | wincmd |
split
1wincmd k
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 78 + 74) / 148)
exe '2resize ' . ((&lines * 19 + 20) / 41)
exe 'vert 2resize ' . ((&columns * 69 + 74) / 148)
exe '3resize ' . ((&lines * 18 + 20) / 41)
exe 'vert 3resize ' . ((&columns * 69 + 74) / 148)
argglobal
if bufexists("term://~/Code/ROS/Patrol/src/termviz/src//914736:/bin/zsh") | buffer term://~/Code/ROS/Patrol/src/termviz/src//914736:/bin/zsh | else | edit term://~/Code/ROS/Patrol/src/termviz/src//914736:/bin/zsh | endif
if &buftype ==# 'terminal'
  silent file term://~/Code/ROS/Patrol/src/termviz/src//914736:/bin/zsh
endif
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
let s:l = 117 - ((37 * winheight(0) + 19) / 38)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
117
normal! 03|
wincmd w
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 9) / 19)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
let s:c = 249 - ((33 * winwidth(0) + 34) / 69)
if s:c > 0
  exe 'normal! ' . s:c . '|zs' . 249 . '|'
else
  normal! 0249|
endif
wincmd w
argglobal
if bufexists("~/Code/ROS/Patrol/src/termviz/include/termviz/DataStructures.h") | buffer ~/Code/ROS/Patrol/src/termviz/include/termviz/DataStructures.h | else | edit ~/Code/ROS/Patrol/src/termviz/include/termviz/DataStructures.h | endif
if &buftype ==# 'terminal'
  silent file ~/Code/ROS/Patrol/src/termviz/include/termviz/DataStructures.h
endif
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 38 - ((14 * winheight(0) + 9) / 18)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
38
normal! 0
wincmd w
exe 'vert 1resize ' . ((&columns * 78 + 74) / 148)
exe '2resize ' . ((&lines * 19 + 20) / 41)
exe 'vert 2resize ' . ((&columns * 69 + 74) / 148)
exe '3resize ' . ((&lines * 18 + 20) / 41)
exe 'vert 3resize ' . ((&columns * 69 + 74) / 148)
tabnext 2
set stal=1
if exists('s:wipebuf') && getbufvar(s:wipebuf, '&buftype') isnot# 'terminal'
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 winminheight=1 winminwidth=1 shortmess=filnxtToOFI
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &g:so = s:so_save | let &g:siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
