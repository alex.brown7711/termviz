/*
 * =====================================================================================
 *
 *       Filename:  MapParser.cpp
 *
 *    Description:  MapParser methods
 *
 *        Version:  1.0
 *        Created:  09/16/2020 05:58:40 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alexander Brown (), alex.brown7711@aggiemail.usu.edu
 *   Organization:  Utah State University
 *
 * =====================================================================================
 */

// Custom Header Files
#include "termviz/MapParser.h"

// System Header Files
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sstream>

using namespace termviz;

/*-----------------------------------------------------------------------------
 * PUBLIC  
 *-----------------------------------------------------------------------------*/


/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: MapParser
 * Description:  Constructor 
 *--------------------------------------------------------------------------------------
 */
MapParser::MapParser(std::string file_path):
  m_file_path(file_path)
{
  std::ifstream m_fh; 
  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: close
 * Description:  Public API to close file handle 
 *--------------------------------------------------------------------------------------
 */
bool MapParser::close()
{
  return closeFile();
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: getFileHandle
 * Description:  Public API to get file handler 
 *--------------------------------------------------------------------------------------
 */
std::ifstream* MapParser::getFileHandle()
{
  return &m_fh;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: parse
 * Description:  Public API to parse YAML file 
 *--------------------------------------------------------------------------------------
 */
std::vector<pointp> MapParser::parse()
{
  if (openFile())
  {
    parseFile();
    closeFile();
  }

  return m_wall;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: ~MapParser
 * Description:  Deconstructor 
 *--------------------------------------------------------------------------------------
 */
MapParser::~MapParser()
{}


/*-----------------------------------------------------------------------------
 * PRIVATE 
 *-----------------------------------------------------------------------------*/

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: openFile
 * Description:  Opens file and returns success 
 *--------------------------------------------------------------------------------------
 */
bool MapParser::openFile()
{
  try
  {
    m_fh.open(m_file_path);
  }
  catch(...)
  {
    printf("ERROR: COULD NOT FIND FILE\n");
  }
  return m_fh.is_open();
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: parseFile
 * Description:  Implementation of parsing algorithm of YAML file 
 *--------------------------------------------------------------------------------------
 */
void MapParser::parseFile()
{
  std::vector<std::string> buff;
  std::string temp;

  for (unsigned int i = 0; getline(m_fh, temp); ++i)
  {
    if (temp.empty()) continue;
    cleanUpFile(temp);
    buff.push_back(temp);
  }                                              /* Clean up file and parse     */

//   std::cout << buff[0] << std::endl;
//   std::cout << buff[1] << std::endl;
//   std::cout << buff[2] << std::endl;
//   std::cout << buff[3] << std::endl;

  strToVec(buff, ',');

//  std::cout << m_wall[0].first.x << " ";
//  std::cout << m_wall[0].first.y << " ";
//  std::cout << m_wall[0].second.x << " ";
//  std::cout << m_wall[0].second.y << std::endl;

  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: closeFile
 * Description:  Implementation of closing the file handle 
 *--------------------------------------------------------------------------------------
 */
bool MapParser::closeFile()
{
  m_fh.close();
  return m_fh.is_open();
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: cleanUpFile
 * Description:  Removes unwanted characters and spaces 
 *--------------------------------------------------------------------------------------
 */
void MapParser::cleanUpFile(std::string& str)
{
  eraseAllSubStr(str, "x1: [");
  eraseAllSubStr(str, "y1: [");
  eraseAllSubStr(str, "x2: [");
  eraseAllSubStr(str, "y2: [");
  eraseAllSubStr(str, "]");
  eraseAllSubStr(str, " ");
  return;
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: eraseAllSubStr
 * Description:  Given a string and a stubstring, erase all instances of substring
 *               in string
 *--------------------------------------------------------------------------------------
 */
void MapParser::eraseAllSubStr(std::string& str, const std::string& sub_str)
{
    size_t pos = std::string::npos;
    // Search for the substring in string in a loop untill nothing is found
    while ((pos = str.find(sub_str) )!= std::string::npos)
    {
        // If found then erase it from string
        str.erase(pos, sub_str.length());
    }
}

/*
 *--------------------------------------------------------------------------------------
 *       Class:  MapParser
 *      Method:  MapParser :: strToVec
 * Description:  Converts a delimited string into a vector 
 *--------------------------------------------------------------------------------------
 */
void MapParser::strToVec(std::vector<std::string>& str, const char& delimiter)
{
  // TODO: Generalize parsing
  // TODO: More efficient parsing

  std::stringstream x1(str[0]);
  std::stringstream y1(str[1]);
  std::stringstream x2(str[2]);
  std::stringstream y2(str[3]);

  while( x1.good() && y1.good() && x2.good() && y2.good())
  {
    std::string x1_str;
    std::string y1_str;
    std::string x2_str;
    std::string y2_str;

    getline( x1, x1_str, delimiter );
    getline( y1, y1_str, delimiter );
    getline( x2, x2_str, delimiter );
    getline( y2, y2_str, delimiter );
      
    pointp temp_line;
    temp_line.first.x  = atof(x1_str.c_str());
    temp_line.first.y  = atof(y1_str.c_str());
    temp_line.second.x = atof(x2_str.c_str());
    temp_line.second.y = atof(y2_str.c_str());


    m_wall.push_back(temp_line);
  }

  return;
}
