/*
 * =====================================================================================
 *
 *       Filename:  termviz.cpp
 *
 *    Description:  This meta-project is to re-familiarize with ROS. 
 *
 *        Version:  1.0
 *        Created:  09/11/2020 11:42:43 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alexander Brown (), alex.brown7711@aggiemail.usu.edu
 *   Organization:  Utah State University
 *
 * =====================================================================================
 */

// Custom Header Files
#include "termviz/TerminalPlotter.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description: main 
 * =====================================================================================
 */
int main(int argc, char** argv)
{
  ros::init(argc, argv, "termviz");

  ros::NodeHandle node;
  termviz::TerminalPlotter term_plot(&node);

  ros::Rate r(1);

  while(ros::ok()) 
  {
    ros::spinOnce();
    term_plot.Plot();
    r.sleep();
    system("clear");
  }

  return 0;
}
